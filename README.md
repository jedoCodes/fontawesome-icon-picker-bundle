![](github_fontawesome.png)


# FontAwesome IconPicker Contao Backend Widget
Add a FontAwesome Iconpicker input element to the Contao backend


## Install & documentation

For documentation please visit https://docs.jedo.codes

## Compatible

Compatible with Contao >=4.9

## dependencies
- [jedocodes/fontawesome-free](https://gitlab.com/jedocodes/fontawesome-free)
