<?php

/**
 * FontAwesome IconPicker bundle for Contao Open Source CMS
 * Copyright (c) 2021 jedo.Codes
 *
 * @category ContaoBundle
 * @package  jedocodes/fontawesome-iconpicker-widget-bundle
 * @author   jedo.Codes <develop@jedo.codes>
 * @link     https://gitlab.com/jedocodes/fontawesome-iconpicker-widget-bundle
 */


namespace JedoCodes\FontawesomeIconpickerWidgetBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Contao\Config;

class FontawesomeIconpickerWidgetBundle extends Bundle
{
    public function getFontAwesomeJavascript(): string
    {

        $FontAwesomeJsSRC = self::getSelectedFontAwesomeSRC();

        return $FontAwesomeJsSRC."/js/all.min.js";

    }

    public function getFontAwesomeStylesheet(): string
    {

        $FontAwesomeJsSRC = self::getSelectedFontAwesomeSRC();

        return $FontAwesomeJsSRC."/css/all.min.css";

    }

    public function getFontAwesomeIconYaml(): string
    {

        $FontAwesomeSRC = self::getSelectedFontAwesomeSRC();


        return $FontAwesomeSRC."/metadata/icons.yml";

    }

    public function getSelectedFontAwesomeSRC(): string
    {

        if (Config::get('customFontawesomeSRC') != '') {

            $objFile = \FilesModel::findById(Config::get('customFontawesomeSRC'));

            $source = $objFile->path;

        } else {

            $source = "assets/fontawesome-free";

        }

        return $source;

    }
}
