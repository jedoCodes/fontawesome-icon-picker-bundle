<?php
/**
 * FontAwesome IconPicker bundle for Contao Open Source CMS
 * Copyright (c) 2021 jedo.Codes
 *
 * @category ContaoBundle
 * @package  jedocodes/fontawesome-iconpicker-widget-bundle
 * @author   jedo.Codes <develop@jedo.codes>
 * @link     https://gitlab.com/jedocodes/fontawesome-iconpicker-widget-bundle
 */

$GLOBALS['TL_LANG']['tl_settings']['fontawesome_legend'] = 'Fontawesome 5 Icon Picker Einstellungen';
$GLOBALS['TL_LANG']['tl_settings']['customFontawesomeSRC'] = array('Pfad zum FontAwesome 5 Ordner (NUR notwendig für die Pro Version)', 'Geben Sie den Pfad zur Javascript Datei an, falls Sie die FontAwesome 5 Pro Version benutzen. (files/fontawesome-pro-web/)');
