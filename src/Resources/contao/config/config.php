<?php
declare(strict_types=1);
/**
 * FontAwesome IconPicker bundle for Contao Open Source CMS
 * Copyright (c) 2021 jedo.Codes
 *
 * @category ContaoBundle
 * @package  jedocodes/fontawesome-iconpicker-widget-bundle
 * @author   jedo.Codes <develop@jedo.codes>
 * @link     https://gitlab.com/jedocodes/fontawesome-iconpicker-widget-bundle
 */

use JedoCodes\FontawesomeIconpickerWidgetBundle\Widget\FontawesomeIconpicker;

define('FONTAWESOME_VERSION', '5.15.4');

if(Config::get('customFontawesomeSRC') != '')
{
    // Use custom version
    $objFile = \FilesModel::findById(\Contao\Config::get('customFontawesomeSRC'));
    $GLOBALS['TL_JAVASCRIPT'][] = $objFile->path.'/css/all.min.css';
}
else
{
    // use the jedoCodes contao-component-fontawesome-free bundle
    $GLOBALS['TL_CSS'][] = 'assets/fontawesome-free/css/all.min.css';

}



if (TL_MODE == 'BE')
{
    $GLOBALS['TL_CSS']['IconPicker'] = 'bundles/fontawesomeiconpickerwidget/css/iconPicker.min.css|static';
    $GLOBALS['TL_JAVASCRIPT']['IconPicker'] = 'bundles/fontawesomeiconpickerwidget/js/iconPicker.min.js';


    if(Config::get('customFontawesomeSRC') != '')
    {
        // Use custom version
        $objFile = \FilesModel::findById(\Contao\Config::get('customFontawesomeSRC'));

        $GLOBALS['TL_JAVASCRIPT'][] = $objFile->path.'/js/all.min.js';
    }
    else
    {
        // use the jedoCodes contao-component-fontawesome-free bundle
        $GLOBALS['TL_JAVASCRIPT'][] = 'assets/fontawesome-free/js/all.min.js';
    }

}


$GLOBALS['BE_FFL']['fontawesomeiconpicker'] = FontawesomeIconpicker::class;

